# inchi_vcpkg #

This is a stripped-down version of inchi core library (v1.0.7-RC1) designed to be used with the CMake build system and vcpkg.

The original repository can be found [here](https://github.com/IUPAC-InChI/InChI.git). Source code is unaltered from the source, and can be replicated
by running:

``` sh
git clone https://github.com/IUPAC-InChI/InChI.git
git checkout v1.07-RC1
```

The contents of the INCHI-1-SRC directory were copied into this repository and the following directories were removed:

- INCHI_EXE
- INCHI_API/bin
- INCHI_API/bin2
- INCHI_API/demos
- INCHI_API/libinchi/vc14
- INCHI_API/libinchi/gcc

The CMakeLists file is a best-guess from looking through the makefile previously located at `INCHI_API/libinchi/gcc/makefile`
